import sys
import ast
import argparse
import collections

def get_argpars():
    parser = argparse.ArgumentParser()
    parser.add_argument('-N',
                        type=int,
                        help='filter by number of result'
                        )
    return parser.parse_args()

def shuffle(data):
    result = {} # result is a dict where key = genre, value = list with many 'film_title;year'
    for line in data:
        key, value = line.split("\t")

        value = value[:-1]

        if key not in result:
            result[key] = []
            result[key].append(value)
        elif key in result:
            result[key].append(value)

    return result

def dict_sorted_by_key(dict_data):
    # sort genres by alphabetically
    sorted_result = sorted(dict_data.items(), key=lambda x: x[0])
    dict_result = dict(sorted_result)
    return dict_result


def dict_sorted_by_values(dict):
    result = {}
    for key in dict:
        result[key] = (sorted(map(ast.literal_eval, dict[key]), key=lambda x: (x[1], x[0]), reverse=True))
    return result


def check_by_n(N, genre, count):
    if N is None:
        return True
    else:
        count[genre] += 1
        if count[genre] <= N:
            return True

def reduce(key, values):
    count = collections.Counter()
    for name, year in values:
        if check_by_n(args.N, key, count):
            yield key, name, year


def print_result(dict):
    print('genre;name;year')
    for key in dict:
        for genre, name, year in reduce(key, dict[key]):
            print('{};{};{}'.format(genre, name, year))




if __name__ == "__main__":
    args = get_argpars()
    my_dict = shuffle(sys.stdin)
    sorted_dict_by_key = dict_sorted_by_key(my_dict)
    sored_dict = dict_sorted_by_values(sorted_dict_by_key)
    print_result(sored_dict)