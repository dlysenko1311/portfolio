# Description:

Dir homework5/mapreduce_hadoop is hadoop's mapreduce project to get movies

***


# **OPTIONS:**

        mapred streaming \
        -input /movies.csv \
        -output /result1 \
        -file ~/coherent-training-denis-lysenko/homework5/mapreduce_hadoop/mapper.py ~/coherent-training-denis-lysenko/homework5/mapreduce_hadoop/reducer.py \
        -mapper "python mapper.py [-genres -year_from -year_to -regexp]" -reducer "python reducer.py [-N]"

    * -N [count of the highest rated films for each genre.] <br>
    * -genres [filter by genre, set by the user. it can be multiple. For example: Comedy|Adventure or Comedy&Adventure.] <br>
    * -year_from [filter for the years of release of films.] <br>
    * -year_to [filter for the years of release of films.] <br>
    * -regexp [filter (regular expression) on the movie name.] <br>

***


# **Usage and first settings:**



**you must locate in cluster's terminal and runs the following commands:**

get a shh key

```
ssh-keygen -t rsa -C denislysenko0001
```

copy shh key
```
cat ~/.ssh/id_rsa.pub
```
you must copy shh key and add this key by this link https://bitbucket.org/account/settings/ssh-keys/ to cloaning project

```
git clone git@bitbucket.org:coherentprojects/coherent-training-denis-lysenko.git
```
now you have a folder with my homeworks on a local machine and you need add movies.csv to hdfs

```
hdfs dfs -put ~/coherent-training-denis-lysenko/homework5/mapreduce_hadoop/files/movies.csv /
```

Great. Now you can run this by following command

```
mapred streaming -input /movies.csv -output /result1 -file ~/coherent-training-denis-lysenko/homework5/mapreduce_hadoop/mapper.py ~/coherent-training-denis-lysenko/homework5/mapreduce_hadoop/reducer.py -mapper "python mapper.py" -reducer "python reducer.py"
```


