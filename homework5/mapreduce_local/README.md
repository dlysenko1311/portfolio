# Description:

Dir homework5/mapreduce_local is local mapreduce project to get movies

***

# **Usage:**


cat files/movies.csv | python3 mapper.py [-genres -year_from -year_to -regexp] | python3 shuffle.py | python3 sort.py | python3 reduce.py [-N]


***

# **OPTIONS:**

    * -N [count of the highest rated films for each genre.] <br>
    * -genres [filter by genre, set by the user. it can be multiple. For example: Comedy|Adventure or Comedy&Adventure.] <br>
    * -year_from [filter for the years of release of films.] <br>
    * -year_to [filter for the years of release of films.] <br>
    * -regexp [filter (regular expression) on the movie name.] <br>

***

# **EXAMPLES:**

```


$  cat files/movies.csv | python3 mapper.py -regexp Terminator -genres "Action|Thriller" | python3 shuffle.py | python3 sort.py | python3 reducer.py -N 3


    # console return:

    genre;name;year
    Action;Terminator Genisys;2015
    Action;Terminator Salvation;2009
    Action;Terminator 3: Rise of the Machines;2003
    Thriller;Terminator Genisys;2015
    Thriller;Terminator Salvation;2009
    Thriller;Terminator, The;1984





$ cat files/movies.csv | python3 mapper.py -year_from 2018 -genres 'Action|Comedy' | python3 shuffle.py | python3 sort.py | python3 reducer.py -N 3


    # console return:

    genre;name;year
    Action;Tomb Raider;2018
    Action;SuperFly;2018
    Action;Solo: A Star Wars Story;2018
    Comedy;When We First Met;2018
    Comedy;Tom Segura: Disgraceful;2018
    Comedy;The Man Who Killed Don Quixote;2018





$  cat files/movies.csv | python3 mapper.py -year_from 2012 -year_to 2017 -genres 'Adventure' -regexp Love | python3 shuffle.py | python3 sort.py | python3 reducer.py -N 2


    # console return:

    genre;name;year
    Adventure;The Lovers;2015
    Adventure;God Loves Caviar;2012



```

***