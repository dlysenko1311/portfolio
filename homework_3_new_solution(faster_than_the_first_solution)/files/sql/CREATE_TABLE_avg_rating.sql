USE movies_db;

CREATE TABLE avg_rating AS

SELECT DISTINCT     movie_id,
                    avg(rating) as avgrating

FROM                rating

GROUP BY            movie_id

ORDER BY            movie_id;