USE movies_db;

CREATE TABLE        result_movies_table AS

SELECT DISTINCT     movies.movie_id,
                    movies.genres,
                    movies.title,
                    movies.year,
                    avg_rating.avgrating

FROM                movies

JOIN                avg_rating

ON                  movies.movie_id = avg_rating.movie_id

ORDER BY            movies.movie_id,
                    movies.genres,
                    avg_rating.avgrating DESC;



DELETE FROM         result_movies_table

WHERE               genres = '(no genres listed)'
                    OR title = ''
                    OR title = '''71 '
                    OR year = 0;