SELECT      genres,
            title,
            year,
            ROUND(avgrating, 2)

FROM        result_movies_table

WHERE
            year BETWEEN {year_from} and {year_to}
            AND title LIKE "%{name}%"
            AND genres LIKE "%{genre}%"

ORDER BY    genres,
            avgrating DESC, year DESC, title

LIMIT       {N};
