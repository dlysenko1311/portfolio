import pandas as pd
import argparse
import pyarrow.parquet

parser = argparse.ArgumentParser()
parser.add_argument('--csv2parquet',
                        nargs=2,
                        help='csv to parquet'
                        )
parser.add_argument('--parquet2csv',
                        nargs=2,
                        help='parquet to csv'
                        )
parser.add_argument('--get_schema',
                        help='to get a schema'
                        )
args = parser.parse_args()


# convert csv to parquet
def csv_to_parquet(src_filename, dst_filename):
    df = pd.read_csv(src_filename)
    df.to_parquet(dst_filename)

# convert parquet to csv
def parquet_to_csv(src_filename, dst_filename):
    df = pd.read_parquet(src_filename)
    df.to_csv(dst_filename)

# get schema of parquetfile
def get_schema(filename):
    schema = pyarrow.parquet.ParquetFile(filename).schema_arrow
    return schema


# !!!!! main condition
def main():
    if args.csv2parquet is not None:
        csv_to_parquet(args.csv2parquet[0], args.csv2parquet[1])
    elif args.parquet2csv is not None:
        parquet_to_csv(args.parquet2csv[0], args.parquet2csv[1])
    elif args.get_schema is not None:
        print(get_schema(args.get_schema))
    else:
        print('type: python3 convert.py --help')

if __name__ == "__main__":
    main()





