**Usage**

python3 convert.py [--csv2parquet | --parquet2csv <src-filename> <dst-filename>] | [--get-schema <filename>] | [--help] ]

You can convert csv to parquet OR parquet to csv OR get schema of parquet file 


OPTIONS:

--csv2parquet  [convert csv to parquet] 

--parquet2csv  [convert parquet to csv] 

--get_schema   [get schema of parquet file] 

--help 		[get help message] 


EXAMPLES: 

python3 convert.py --csv2parquet files/example.csv files/example.parquet 

python3 convert.py --parquet2csv files/example.parquet files/example.csv 

python3 convert.py --get_schema files/example.parquet 

python3 convert.py --help 
