import argparse
from config import host, user, passwd, db_name, default_genres
import pymysql
from pymysql.constants import CLIENT


def argpars():
    parser = argparse.ArgumentParser()
    parser.add_argument('-N',
                        type=int,
                        help='the number of the highest rated films for each genre',
                        default=9724
                        )
    parser.add_argument('-genres',
                        type=str,
                        help='filter by genre',
                        default = default_genres
                        )
    parser.add_argument('-year_from',
                        type=int,
                        help='filter by year (FROM YEAR)',
                        default=1800
                        )
    parser.add_argument('-year_to',
                        type=int,
                        help='filter by year (TO YEAR)',
                        default=2030
                        )
    parser.add_argument('-regexp',
                        type=str,
                        help='filter on the movie name',
                        default=''
                        )
    return parser.parse_args()


def get_connection_to_db(host, user, passwd, db_name):
    connection = None
    try:
        connection = pymysql.connect(
            host = host,
            user = user,
            passwd = passwd,
            database = db_name,
            client_flag = CLIENT.MULTI_STATEMENTS
        )
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection


def print_result(connection, sql_script_path, args):
        print('genre;title;year;rating')
        genres = args.genres.split("|")
        for genre in genres:
            with connection.cursor() as cursor:
                with open(sql_script_path) as file:
                    script = file.read()
                    cursor.execute(script.format(year_from=args.year_from,
                                                 year_to=args.year_to,
                                                 name=args.regexp,
                                                 genre=genre,
                                                 N=args.N))
                    rows = cursor.fetchall()
                    for row in rows:
                        print('{};{};{};{}'.format(genre, row[0], row[1], row[2]))



if __name__ == "__main__":
    args = argpars()
    connection = get_connection_to_db(host, user, passwd, db_name)
    print_result(connection, './files/sql/GET_RESULT_MOVIES.sql', args)




