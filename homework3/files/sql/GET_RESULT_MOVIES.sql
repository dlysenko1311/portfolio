SELECT DISTINCT title, year, rating
FROM movies as m
JOIN rating as r
ON m.movie_id = r.movie_id
WHERE m.year BETWEEN {year_from} and {year_to} AND m.title LIKE "%{name}%" AND m.genres LIKE "%{genre}%"
ORDER BY rating DESC
LIMIT {N};
