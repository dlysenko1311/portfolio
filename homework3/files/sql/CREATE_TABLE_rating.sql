USE my_movies_db;

CREATE TABLE rating (
id              INT                 AUTO_INCREMENT,
movie_id        INT(20)             NOT NULL,
rating          DECIMAL(3, 2),
PRIMARY KEY     (id)
);