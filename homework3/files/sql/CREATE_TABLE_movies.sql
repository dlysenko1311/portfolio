USE my_movies_db;

CREATE TABLE movies (
movie_id        INT,
title           VARCHAR(256),
year            INT,
genres          TEXT,
PRIMARY KEY     (movie_id)
);
