import argparse
import csv
import collections
import re

def get_argpars():
    parser = argparse.ArgumentParser()
    parser.add_argument('-genres',
                        type=str,
                        help='filter by genre',
                        default=''
                        )
    parser.add_argument('-year_from',
                        type=int,
                        help='filter by year (FROM YEAR)',
                        default=1800
                        )
    parser.add_argument('-year_to',
                        type=int,
                        help='filter by year (TO YEAR)',
                        default=2025
                        )
    parser.add_argument('-regexp',
                        type=str,
                        help='filter on the movie name',
                        default=''
                        )
    parser.add_argument('--N',
                        type=int,
                        help='filter by number of result'
                        )
    return parser.parse_args()


def get_list_from_csv_file(file_path):
    with open(file_path, encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=',')
        data = list(reader)
    return data[1:]


def get_averages():
    total = collections.defaultdict(float)
    count = collections.defaultdict(int)
    for line in get_list_from_csv_file('files/ratings.csv'):
        total[line[1]] += float(line[2])
        count[line[1]] += 1
    # middle rating
    averages = { id: total[id]/count[id] for id in count }

    return averages


def get_movies_list(averages):
    result = []
    for movies_id, genre, name, year in filter_list_line_by_cmd_arguments(get_list_from_csv_file('files/movies.csv')):
        result.append([genre, name, year, get_and_check_rating(movies_id, averages)])
    # sort by genres, ratings, years and names
    result.sort(key=lambda row: (row[0], -row[3], -row[2], row[1]))

    return result


def filter_by_year(year_from, year_to, string):
    pattern = r'\(\d{4}\)'
    if re.search(pattern, string):
        year = int(re.search(r'\(\d{4}\)', string).group(0)[1:-1])
        if year_from <= year <= year_to:
            return True


def filter_by_regexp(name, string):
    pattern = name
    if re.search(pattern, string):
        return True


def filter_by_genres(genre, arg_genre):
    if arg_genre == '' and genre != '(no genres listed)':
        return True
    elif genre == arg_genre and genre != '(no genres listed)':
        return True


def get_and_check_rating(movies_id, rating_dict):
    if movies_id in rating_dict:
        return round(rating_dict[movies_id], 1)
    else:
        return 0.1


def check_by_n(N, genre, count):
    if N is None:
        return True
    else:
        count[genre] += 1
        if count[genre] <= N:
            return True


def filter_list_line_by_cmd_arguments(data):
    for movie_id, movie_name, movies_genres in data:
        # filter by year and regexp
        if filter_by_year(args.year_from, args.year_to, movie_name) and filter_by_regexp(args.regexp, movie_name):
            # filter by genre
            for arg_genre in args.genres.split('|'):
                for genre in movies_genres.split('|'):
                    if filter_by_genres(genre, arg_genre):
                        name = movie_name[:-7]
                        year = int(re.search(r'\(\d{4}\)', movie_name).group(0)[1:-1])
                        yield movie_id, genre, name, year


def print_result(data):
    print('genre;name;year;rating')
    count = collections.Counter()
    for genre, name, year, rating in data:
        if check_by_n(args.N, genre, count):
            print('{};{};{};{}'.format(genre, name, year, rating))


if __name__ == "__main__":
    args = get_argpars()
    averages = get_averages()
    result_list = get_movies_list(averages)
    print_result(result_list)




