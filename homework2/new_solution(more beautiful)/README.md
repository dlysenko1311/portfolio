# Description:

Dir homework2/new_solution(more beautiful) **is my better solution which I decided to do after mapreduce homework, because by that time I had a lot of new knowledge**. This is a command-line utility that determines the most rated films (by average rating) for each specified genre (if the genre is passed as an argument of the command line).
The utility return to the console: genre, title, year of release and rating.

***

# **Usage:**

python3 get-movies.py [--N <int>] | [-genres <name_genre|name_genre>] | [-year_from <int> ] | [-year_to <int> ] | [-regexp <films name>]


***

# **OPTIONS:**

    * --N [count of the highest rated films for each genre.] <br>
    * -genres [filter by genre, set by the user. it can be multiple. For example: Comedy|Adventure or Comedy&Adventure.] <br>
    * -year_from [filter for the years of release of films.] <br>
    * -year_to [filter for the years of release of films.] <br>
    * -regexp [filter (regular expression) on the movie name.] <br>

***

# **EXAMPLES:**

```

* python3 get-movies.py -year_from 2000 -year_to 2001  -genres 'Comedy' --N 3


    # console return:

    genre;name;year;rating
    Comedy;My Life as McDull (Mak dau goo si);2001;5.0
    Comedy;My Sassy Girl (Yeopgijeogin geunyeo);2001;5.0
    Comedy;Son of the Bride (Hijo de la novia, El);2001;5.0




*  python3 get-movies.py -year_from 1999 -year_to 2001 -genres 'Adventure|Comedy' --N 2


    # console return:

    genre;name;year;rating
    Adventure;Musa the Warrior (Musa);2001;4.5
    Adventure;Dragonheart 2: A New Beginning;2000;4.5
    Comedy;My Life as McDull (Mak dau goo si);2001;5.0
    Comedy;My Sassy Girl (Yeopgijeogin geunyeo);2001;5.0




*  python3 get-movies.py -genres 'Action' -regexp Terminator --N 4


    # console return:

    genre;name;year;rating
    Action;Terminator 2: Judgment Day;1991;4.0
    Action;Terminator, The;1984;3.9
    Action;Terminator Salvation;2009;3.2
    Action;Terminator 3: Rise of the Machines;2003;3.0

```

***