import argparse
import csv
from collections import defaultdict
import re

def argpars():
    parser = argparse.ArgumentParser()
    parser.add_argument('-N',
                        type=int,
                        help='the number of the highest rated films for each genre'
                        )
    parser.add_argument('-genres',
                        type=str,
                        help='filter by genre'
                        )
    parser.add_argument('-year_from',
                        type=int,
                        help='filter by year (FROM YEAR)',
                        default=1800
                        )
    parser.add_argument('-year_to',
                        type=int,
                        help='filter by year (TO YEAR)',
                        default=2025
                        )
    parser.add_argument('-regexp',
                        type=str,
                        help='filter on the movie name'
                        )
    return parser.parse_args()


# read movies.csv
def read_movies_file():
    data_m = []
    with open('files/movies.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            data_m.append(row)
    data_movies = data_m[1:]
    return data_movies


# read ratings.csv
def read_rating_file():
    data_r = []
    with open('files/ratings.csv', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            data_r.append(row)
    data_rating = data_r[1:]
    return data_rating



# get averages, is a dict in which: key=str(movieID), values=middle rating.
# and sort sort list by rating (reverse=True)
def get_averages():
    total = defaultdict(float)
    count = defaultdict(int)
    for line in data_rating:
        total[line[1]] += float(line[2])
        count[line[1]] += 1
    # middle rating
    averages = { id: total[id]/count[id] for id in count }
    # sort list by reverse=True
    data_movies.sort(key=lambda item: averages.get(item[0], 0), reverse=True)
    return averages



# make a list from geners
def make_list_from_geners_in_data_movies():
    for i in range(len(data_movies)):
        geners_list = data_movies[i][2].split('|')
        data_movies[i][2] = geners_list
    return data_movies


"""
Now we have a table data_movies, which sorted by rating (Reverse=True)
Below are the sorting functions
"""



def filter_by_year(year_from, year_to):
    result = []
    pattern = r'\(\d{4}\)'
    for i in range(len(data_movies)):
        string = data_movies[i][1]
        if re.search(pattern, string) is not None:
            year = re.search(pattern, string)
            a = year.group(0)[1:-1]
            int_year = int(a)
            if year_from <= int_year <= year_to:
                result.append(data_movies[i])
    return result


def filter_by_regexp(name, data):
    result = []
    pattern = name
    for i in range(len(data)):
        string = data[i][1]
        if re.search(pattern, string) is not None:
            result.append(data[i])
    return result


def filter_by_genres(geners, data):
    result = []
    if '|' in geners:
        geners_list = geners.split('|')
    elif '&' in geners:
        geners_list = geners.split('&')
    else:
        geners_list = geners.split()

    for i in range(len(geners_list)):
        gener = geners_list[i]
        for i in range(len(data)):
            if gener in data[i][2]:
                if data[i] not in result:
                    result.append(data[i])
    return  result


def print_result_to_console(data, geners, N):
    print('genre;title;year;rating')
    if N is not None and geners is not None:
        if '|' in geners:
            geners_list = geners.split('|')
        elif '&' in geners:
            geners_list = geners.split('&')
        else:
            geners_list = geners.split()
        # geners_list is genres array

        used_movies = []
        result = []
        for i in range(len(geners_list)):
            # geners_list[i] for example this can be action
            for j in range(len(data)):
                if geners_list[i] in data[j][2]:
                    if data[j] not in used_movies:
                        if len(result) < N:
                            result.append(data[j])
                            used_movies.append(data[j])
            for m in range(len(result)):
                print('{};{};{};{}'.format(geners_list[i], result[m][1][:-6], result[m][1][-5:-1], averages[result[m][0]]))
            result.clear()


    elif N is not None and geners is None:
        data.sort(key=lambda row: row[2])  # main rules of sort: gener, rating.
        for row in data[:N]:
            print('{};{};{};{}'.format(row[2][0], row[1][:-6], row[1][-5:-1], averages[row[0]]))

    elif N is None and geners is not None:
        used_movies = []
        if '|' in geners:
            geners_list = geners.split('|')
        elif '&' in geners:
            geners_list = geners.split('&')
        else:
            geners_list = geners.split()
        # geners_list is genres array

        for i in range(len(geners_list)):
            for j in range(len(data)):
                if geners_list[i] in data[j][2]:
                    if data[j] not in used_movies:
                        print('{};{};{};{}'.format(geners_list[i], data[j][1][:-6], data[j][1][-5:-1], averages[data[j][0]]))

    elif N is None and geners is None:
        data.sort(key=lambda row: row[2])
        for row in data:
            print('{};{};{};{}'.format(row[2][0], row[1][:-6], row[1][-5:-1], averages[row[0]]))




def main_condition():
    result = []
    result = filter_by_year(args.year_from, args.year_to)

    if args.regexp is not None:
        result = filter_by_regexp(args.regexp, result)

    if args.genres is not None:
        result = filter_by_genres(args.genres, result)
    for row in result:
        # check value in averages, if averages dont have a key averages will return value = 0
        if row[0] not in averages:
            averages[row[0]] = 0
    print_result_to_console(result, args.genres, args.N)


if __name__ == "__main__":
    args = argpars()
    data_movies = read_movies_file()
    data_rating = read_rating_file()
    averages = get_averages()
    data_movies = make_list_from_geners_in_data_movies()
    main_condition()
