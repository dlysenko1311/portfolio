# Description:

Dir homework2 is a command-line utility that determines the most rated films (by average rating) for each specified genre (if the genre is passed as an argument of the command line).
The utility return to the console: genre, title, year of release and rating.

***

# **Usage:**

python3 get-movies.py [-N <int>] | [-genres <name_genre|name_genre>] | [-year_from <int> ] | [-year_to <int> ] | [-regexp <films name>]


***

# **OPTIONS:**

    * -N [count of the highest rated films for each genre.] <br>
    * -genres [filter by genre, set by the user. it can be multiple. For example: Comedy|Adventure or Comedy&Adventure.] <br>
    * -year_from [filter for the years of release of films.] <br>
    * -year_to [filter for the years of release of films.] <br>
    * -regexp [filter (regular expression) on the movie name.] <br>

***

# **EXAMPLES:**

```

* python3 get-movies.py -year_from 2000 -year_to 2001  -genres 'Comedy' -N 3


    # console return:

    genre;title;year;rating
    Comedy;Bossa Nova ;2000;5.0
    Comedy;I'm the One That I Want ;2000;5.0
    Comedy;Son of the Bride (Hijo de la novia, El) ;2001;5.0



*  python3 get-movies.py -year_from 1999 -year_to 2001 -genres 'Adventure|Comedy' -N 2


    # console return:

    genre;title;year;rating
    Adventure;Musa the Warrior (Musa) ;2001;4.5
    Adventure;Dragonheart 2: A New Beginning ;2000;4.5
    Comedy;Asterix & Obelix vs. Caesar (Astérix et Obélix contre César) ;1999;4.0
    Comedy;Monsters, Inc. ;2001;3.87



*  python3 get-movies.py -regexp Love -N 4


    # console return:

    genre;title;year;rating
    Comedy;Thin Line Between Love and Hate, A ;1996;5.0
    Action;Love Exposure (Ai No Mukidashi) ;2008;5.0
    Drama;Only Lovers Left Alive ;2013;5.0
    Comedy;One I Love, The ;2014;5.0

```

***